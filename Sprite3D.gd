extends Sprite3D


var opacity_slider : Slider

func _ready():
	opacity_slider = get_node("Slider")
	self.modulate = Color(1, 1, 1, opacity_slider.value)

func _on_opacity_changed(value):
	self.modulate.a = value

func _on_HSlider_changed():
	opacity_slider.connect("value_changed", self, "_on_opacity_changed")


func _on_HSlider_value_changed(value):
	opacity_slider.connect("value_changed", self, "_on_opacity_changed")
